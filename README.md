# plantDataCollector

Aplicativo Shiny (RStudio) para coletores botânicos, taxonomistas e ecólogos organizarem seus dados brutos anotados em cadernetas, aparelhos de GPS, máquinas fotográficas. 

O aplicativo segue a estrutura lógica da base de dados relacional [OpenDataBio](https://github.com/opendatabio) com o qual será integrado futuramente. Nessa lógica há quatro objetos principais: **coletas**, **plantas**, **localidades** e **taxa**. Além das relações entre esses objetos, é possível relacionar qualquer variável (**trait**) com qualquer um desses objetos, criando diferentes **datasets**. Assim, por exemplo, num estudo que usa parcelas amostrais, dados de solo são relacionádos às parcelas (**localidades**), monitoramento fenológico ou de crescimento são relacionados às **plantas** nessas parcelas, o material testemunho depositado em herbário são as **coletas**, medições, imagens e outros dados dos indivíduos podem ser relacionados à **plantas** e/ou **coletas**. Num estudo taxonômico, um revisor pode baixar dados do GBIF para seu grupo, identificar duplicatas e validar coordenadas, e relacionar medições morfológicas e outras variáveis à essas amostras criando um **dataset**. Dados secundários da literatura podem ser relacionados à **taxa** ou qualquer outro objeto, ligando diretamente à referência bibiligráfica (**bibrefs**).

O aplicativo não depende do *OpenDataBio* e pode ser independentemente dele. Ele visa ajudar pesquisadores e principalmente estudantes de pós-graduação que estudedam plantas a organizar melhor seus dados, criando tabelas que podem ser relacionadas com facilidade e minimizando erros de digitação, grafia, etc.

## Funções implementadas
1.  Cadastrar **coletas** botânicas novas
2.  Baixar biblioteca de taxa de *Tropicos.org* via TNRS. Cadastrar morfotipos para nomes temporários.
4.  Subir um arquivo de GPS formato *.gpx com pontos marcados em campo. Atribuir esses pontos diretamente às coletas e obter automaticamente as categorias administrativas (País,Estado,Município) pelas coordenadas geográficas (usando GDAM, restrito ao continente Americano)
5.  Cadastrar **localidades**, de forma hierárquica, pontos ou polígonos usando [Well-known text representation of geometry](https://en.wikipedia.org/wiki/Well-known_text_representation_of_geometry)
6.  Cadastrar **plantas** em localidades
5.  Subir imagens brutas e facilitar ligá-las às coletas (ou localidades e plantas ou taxa).
7.  Cadastrar variáveis para coleta de qualquer tipo de dado categórico, quantitativo, semi-quantitativo, texto com diferentes opções
8.  Criar **datasets** que permite registrar valores para das variávies criadas à qualquer **coleta**, **planta**, **taxa**, ou **localidade**
9.  Importar registros de [bibtex](https://en.wikipedia.org/wiki/BibTeX) e disponibilizar eles na forma de uma variável que pode ser incluída em um **dataset**
11. Baixar dados do GBIF de qualquer taxa e procurar por possíveis duplicatas (mesmo PRESERVED_SPECIMEN depositado em diferentes herbários). Permitir registro de variáveis num **dataset** aos dados GBIF importados.
10. Baixar todos os dados com **identificadores** compatíveis.

## Funções planejadas

1.  Validação das coordenadas geográficas dos dados GBIF - (1) comparar predição de categorias administrativas com aquela informada; (2) para os erros em (1) cálculo da distância às unidades administrativas (minorarea) informadas;  
2.  Importação de planilhas externas para dados digitalizados
3.  Ligação com [OpenDataBio](https://github.com/opendatabio) para envio direto à repositório

## Para instalar

*  Baixar ou clonar este aplicativo
*  Instalar os pacotes do R necessários (ver e/ou executar arquivo *InstallPackages.R*)

## Para rodar

*  Basta rodar o arquivo Ui.R ou Server.R no RStudio
*  Para fazer um backup, basta salvar a pasta inteira do aplicativo em algum lugar (ou se quiser apenas as pastas _refs_ _gbif_ _imgs_ e _datasets_)

**ATENÇÃO** - O aplicativo não é uma base relacional e alguns controles para evitar perda de informação não foram implementados (ou seja se apagar um dado de uma tabela que está ligado a outro, pode perder informação porque todos os controles não foram ainda implementados, então use com atenção).


